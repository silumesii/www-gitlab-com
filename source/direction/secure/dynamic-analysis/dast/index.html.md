---
layout: markdown_page
title: "Category Direction - Dynamic Application Security Testing"
description: "Dynamic application security testing (DAST) is a process of testing an application or software product in an operating state. Learn more here!"
canonical_path: "/direction/secure/dynamic-analysis/dast/"
---

- TOC
{:toc}

## Description

### Overview

Dynamic application security testing (DAST) is a process of testing an application or software product in an operating state. 

When an application has been deployed and started, DAST connects to the published service via its standard web port and performs a scan of the entire application. It can enumerate pages and verify if well-known attack techniques, like cross-site scripts or SQL injections are possible. DAST will also scan REST APIs using an OpenAPI specification as a guide for testing the API endpoints.

DAST doesn't need to be language specific, since the tool emulates a web client interacting with the application, as an end user would.

### Goal

Our goal is to provide DAST as part of the standard development process. This means that DAST is executed every time a new commit is pushed to a branch. We also include DAST as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Since DAST requires a running application, we can provide results for feature branches leveraging [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/), temporary environments that run the modified version of the application.

DAST results can be consumed in the merge request, where only new vulnerabilities, introduced by the new code, are shown. A full report is available in the pipeline details page.

DAST results will also be part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can check the security status.

We also want to ensure that the production environment is always secure, by running an on-demand DAST scan on the deployed app even if there is no change in the code. On-demand scans will allow for out-of-band security testing and issue reproduction, without needing any code changes or merge requests to start a scan.

### Roadmap

- [DAST Viable maturity (already shipped)](https://docs.gitlab.com/ee/user/application_security/dast/)
- [DAST On-demand scans](https://gitlab.com/groups/gitlab-org/-/epics/3053)
- [DAST UI configuration](https://gitlab.com/groups/gitlab-org/-/epics/3323)
- [DAST Complete maturity improvements](https://gitlab.com/groups/gitlab-org/-/epics/2912)
- [DAST Lovable maturity improvements](https://gitlab.com/groups/gitlab-org/-/epics/3207)

## What's Next & Why

The next feature MVC for DAST is to support on-demand scans. Since DAST scanning is not always prompted by a code change, we want to be able to support workflows that require scans without a merge request. The MVC for this feature will allow for a 1-minute passive scan against a target website.
- [DAST On-demand scan MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/216876)

After introducting the on-demand scan MVC, we will begin to improve it by allowing for more configuration options, including changing the length of time the scan can run, the type of scan that is run, and attaching the results to a branch other than the default branch.
- [DAST On-demand scans improvements](https://gitlab.com/gitlab-org/gitlab/-/issues/214025)

We want to improve the DAST configuration process by adding more options for configuration and moving most configuration to the GitLab UI. There are several initiatives in process to enable this by improving the backend configuration and adding the UI configuration options.  The UI configuration options will be available as profiles that can be mixed and matched to make it easier to run different types of DAST scans against a single project. Providing the configurations as profiles will make it easy for a single project to have both targeted and full scans, API and site scans, and passive and active scans all available to run with a couple of clicks.
- [DAST Site configuration profiles](https://gitlab.com/gitlab-org/gitlab/-/issues/217015)
- [DAST Scan configuration profiles](https://gitlab.com/gitlab-org/gitlab/-/issues/217016)
- [DAST UI configuration epic](https://gitlab.com/groups/gitlab-org/-/epics/3323)
- [DAST backend configuration improvements epic](https://gitlab.com/groups/gitlab-org/-/epics/3019)

## Maturity Plan
 - [Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/2912)
 - [Complete to Lovable](https://gitlab.com/groups/gitlab-org/-/epics/3207)

## Competitive Landscape

- [CA Veracode](https://www.veracode.com/products/dynamic-analysis-dast)
- [Microfocus Fortify WebInspect](https://software.microfocus.com/en-us/products/webinspect-dynamic-analysis-dast/overview)
- [IBM AppScan](https://www.ibm.com/security/application-security/appscan)
- [Rapid7 AppSpider](https://www.rapid7.com/products/appspider)
- [Detectify](https://detectify.com/)

We have an advantage of being able to provide testing results before the app is deployed into the production environment, by using Review Apps. This means that we can provide DAST results for every single commit. The easy integration of DAST early in the software development life cycle is a unique position that GitLab has in the DAST market. Integrating other tools at this stage of the SDLC is typically difficult, at best.

## Analyst Landscape

We want to engage analysts to make them aware of the security features already available in GitLab. They also perform analysis of vendors in the space and have an eye on the future. We will blend analyst insights with what we hear from our customers, prospects, and the larger market as a whole to ensure we’re adapting as the landscape evolves. 

* [2019 Gartner Quadrant: Application Security Testing](https://www.gartner.com/en/documents/3906990/magic-quadrant-for-application-security-testing)
* [Gartner Application Security Testing Reviews](https://www.gartner.com/reviews/market/application-security-testing)
* [2019 Forester State of Application Security](https://www.forrester.com/report/The+State+Of+Application+Security+2019/-/E-RES145135)

## Top Customer Success/Sales Issue(s)

- [Full list of DAST issues with the "customer" label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=customer&label_name[]=Category%3ADAST)

## Top user issue(s)

- [Full list of DAST issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST)

## Top internal customer issue(s)

- [Full list of DAST issues with the "internal customer" label](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADAST&label_name[]=internal%20customer)

## Top Vision Item(s)

- [On-demand DAST scans](https://gitlab.com/gitlab-org/gitlab/-/issues/214025)
- [DAST Site configuration profiles](https://gitlab.com/gitlab-org/gitlab/-/issues/217015)
- [DAST Scan configuration profiles](https://gitlab.com/gitlab-org/gitlab/-/issues/217016)

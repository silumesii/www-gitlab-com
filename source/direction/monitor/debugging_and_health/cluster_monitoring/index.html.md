---
layout: markdown_page
title: "Category Direction - Cluster Monitoring"
---

- TOC
{:toc}

## Cluster Monitoring
While we implemented a nice MVC of cluster monitoring, we have some room to improve this. In this iteration, we will focus on providing support for alerts, and adding additional metrics which can cause issues.

Some important priorities:
1. Generate alerts when there are conditions admins need to be aware of, like running out of resources
1. Display resource `requests`, as these can also generate errors when exceeding capacity
1. Display pod summary information, like number of pods per node and total. There is a limitation of 100 pods per node, before they become unschedulable.

## What's Next & Why
Not yet, but accepting merge requests to this document.

## Maturity Plan
* [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/140)

## Competitive Landscape
Not yet, but accepting merge requests to this document.

## Analyst Landscape
Not yet, but accepting merge requests to this document.

## Top Customer Success/Sales Issue(s)
Not yet, but accepting merge requests to this document.

## Top Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Internal Customer Issue(s)
Not yet, but accepting merge requests to this document.

## Top Vision Item(s)
Not yet, but accepting merge requests to this document.

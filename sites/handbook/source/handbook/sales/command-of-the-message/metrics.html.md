---
layout: handbook-page-toc
title: "Demystifying the Metrics Conversation"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Many sales reps find the Metrics portion of the Command of the Message customer conversation to be a difficult one for one or more of the below reasons.
- Many customers don’t have good metrics
- Customers may be reluctant to share metric details for negotiation purposes (they don't want to admit how compelling of an ROI GitLab can provide!)
- Challenge with finding the perfect proof point(s) to demonstrate how we've helped other organizations
- Difficulty connecting Metrics with Positive Business Outcomes (PBOs)
- Metrics, if used in the wrong way, can get complex and technical

To learn more, check out the Force Management blog, ["Why Sales Reps Struggle with Metrics in the Sales Conversation."](https://www.forcemanagement.com/blog/why-sales-reps-struggle-with-metrics-in-the-sales-conversation)

The following sections outline practical tips to help demystify the Metrics conversation and overcome the challenges identified above. 

## Embrace Your Role as the Trusted Advisor
- Customers may not know why their DevOps initiatives are not delivering desired or optimal results
- Help them diagnose and lead with insights to help them accomplish their Positive Business Outcomes (PBOs)

## Demonstrate Empathy
Below is an example of how Nico Ochoa (Strategic Account Leader, US West) opens some of his conversations to connect with his customers to try to understand what challenges they are experiencing. 

“If you’re like other organizations we speak with, below are some of the common challenges they encounter in their software development life cycle:
1. There is still significant room for improvement in their DevOps processes
1. They lack visibility across their entire Software Development Life Cycle
1. They are unable to clearly measure and assess developer productivity
1. Silos persist between Development, Security, and Operations teams
Are any of these true for your organization?”

![empathy-word-cloud](/handbook/sales/command-of-the-message/empathy-word-cloud.png)

## Be a Good Storyteller
The best Metrics conversations are **stories** that articulate quantifiable measurements and proof of the business benefits of the solution to the customer’s problem(s). Examples:
- John May (Strategic Account Leader, US East) leverages this [DORA State of DevOps 2019 slide](https://docs.google.com/presentation/d/1cNcqXbcYR5pEfz-t8_VBE86fqdma_Jc7ws5Icm9FDKY/edit?usp=sharing) to emotionally appeal to individuals' desire to become elite performers and highlights relevant success stories of organizations ([Ticketmaster](/blog/2017/06/07/continous-integration-ticketmaster/), [Verizon Connect](https://www.youtube.com/watch?v=zxMFaw5j6Zs), [Paessler AG](/customers/paessler/), and more) that have accomplished this with GitLab
- Nico Ochoa (Strategic Account Leader, US West) explains the value of GitLab by talking in terms of a software factory and applying lean manufacturing principles. More specifically, GitLab applies these principles to take waste out of the process of developing and deploying applications to increase operational efficiencies, deliver better products faster, and reduce security and compliance risk.

## Know the Difference Between Economic and Technical Metrics

### Economic Metrics
**Economic metrics** highlight how your solution attaches to the customer's large strategic initiatives.
* Economic metrics typically align to one or more of three [customer value drivers](/handbook/sales/command-of-the-message/#customer-value-drivers)
* Check out the [Forrester Total Economic Impact<sup>TM<sup>](/handbook/marketing/product-marketing/analyst-relations/forrester-tei/) study

### Technical Metrics
**Technical metrics** highlight measurable results that prove the solution's capabilities.
* Review [Getting Started with Agile Metrics](/handbook/marketing/product-marketing/devops-metrics/)

### Bringing It Together

To better navigate Metrics conversations, listen to the 7-minute Force Management [Metrics in the Sales Conversation podcast](https://soundcloud.com/force-management-1/metrics-in-the-sales-conversation) and implement these practices:
1. Recognize that your solution impacts decision-makers in different ways and their reasoning for choosing your solution over alternatives is also different. 
1. Understand what each stakeholder/decision-maker cares about and help them (especially your Champions) understand how your solution impacts
    - What they care about, and 
    - What other stakeholders/decision-makers care about
1. Be sure you understand what technical metrics align to the value drivers your customer cares about most (see next section)

## Connect Metrics with Positive Business Outcomes
Link your **technical buyers** to the metrics and their role in driving the positive business outcomes. If you can emotionally connect those technical metrics-focused buyers on the impact they have for the entire company, they will be better able to internally sell the importance of your solution. With **economic or non-technical buyers**, lead with and continue to reinforce the Positive Business Outcomes your solution will deliver and help them understand how those Positive Business Outcomes are associated with the project's technical metrics.

| Value Driver | Metrics |
| ------ | ------ |
| **Increase Operational Efficiencies** | - Lower costs (license, FTEs and compute) of development<br> - Increase developer/team productivity<br> - Reduce cycle time (lead time)<br> - Increase [deployment frequency](/handbook/marketing/product-marketing/devops-metrics/#deployment-frequency)<br> - Reduce [change failure rate](/handbook/marketing/product-marketing/devops-metrics/#change-failure-rate-cfr)<br> - Reduce [MTTR (mean time to recover/restore)](/handbook/marketing/product-marketing/devops-metrics/#mean-time-to-recover-mttr)<br> - Improve developer satisfaction and retention |
| **Deliver Better Products Faster** | - Improve revenue, # of customers, and/or market share<br> - Increase developer/team productivity<br> - Reduce cycle time (lead time)<br> - Increase [deployment frequency](/handbook/marketing/product-marketing/devops-metrics/#deployment-frequency)<br> - Reduce [change failure rate](/handbook/marketing/product-marketing/devops-metrics/#change-failure-rate-cfr)<br> - Reduce [MTTR (mean time to recover/restore)](/handbook/marketing/product-marketing/devops-metrics/#mean-time-to-recover-mttr) |
| **Reduce Security and Compliance Risk** | - Decrease % of critical security vulnerabilities that escaped development<br> - increase the % of code scanned<br> - improve audit pass rate<br> - Reduce time spent on audits and/or compliance adherence efforts<br> - Reduce [MTTR (mean time to recover/restore)](/handbook/marketing/product-marketing/devops-metrics/#mean-time-to-recover-mttr) of security vulnerabilities |

## Know Your Proof Points
* Reference the proof points listed in the GitLab Value Framework (see the [Command of the Message Core Content](/handbook/sales/command-of-the-message/#resources-core-content)) 
* Familiarize yourself with GitLab's [Proof Points](/handbook/sales/command-of-the-message/proof-points.html) to the point where you can talk about various success stories in a conversational manner

## Embrace the Tension
* Early in the sales process, a difficult metrics conversation is a good sign that the customer wants and needs a trusted advisor (you!!) to help them with their DevOps adoption journey. Leverage relevant proof points to instill confidence that GitLab is uniquely suited to help them achieve their desired outcomes.
* As the opportunity progresses, customers will ask for and expect discounting because that’s what they’ve been trained to do. Maintain conviction in and continue to reiterate the VALUE of the GitLab solution and the defensible differentiators that make GitLab the best choice.

## Leverage Metrics to Create Urgency
By understanding your customer's desired outcomes and their strategic business initiatives, be sure you fully understand and can articulate the answer to the following questions:
1. Why should the customer buy anything?
1. Why should the customer buy now?
1. Why should the customer buy the GitLab solution?
1. What is the cost and impact to the customer of doing nothing?
1. Is there a compelling event that impacts any of the above?

And once you have identified compelling answers to these questions, continue to reiterate and reinforce that value with key influencers and decision makers (keeping in mind that different stakeholders may care about different things).

## Assess the Health of Your Metrics
Periodically, conduct a critical review of the below questions. This can be done via a self-assessment and/or with your account team, peers, mentors, or your manager. In some instances, you may even consider reviewing this with your Champion.  
1. Has the customer bought into the identified metrics?
1. Are the metrics aligned with the desired business outcomes?
1. Are your Champions leveraging the identified metrics?
1. Are the identified metrics in the customer's business justification and/or decision criteria?
1. Has the economic buyer made reference calls based on the identified metrics?

## Practice, Practice, Practice
Purposeful practice (combined with on-the-job experience) leads to skill mastery. [Read the article](https://www.mikekunkle.com/2018/04/08/sales-leaders-its-time-to-get-serious-about-purposeful-practice-skill-mastery/). Michael Jordan became the greatest basketball player of all time, at least in part, because he worked hard at practicing and perfecting his craft. Practice having the metrics conversation with yourself, with a peer, a mentor, your manager, or other. As you become more comfortable and confident, positive results will follow.
